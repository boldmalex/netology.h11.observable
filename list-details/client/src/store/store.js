import { applyMiddleware, combineReducers, createStore } from "redux";
import { combineEpics, createEpicMiddleware } from "redux-observable";
import serviceReducer from "./services/service-reducer";
import servicesReducer from "./services/services-reducer";
import serviceEpics from "./services/service-epics";

const reducer = combineReducers({servicesReducer, serviceReducer});

const epic = combineEpics(
    serviceEpics.serviceRequestEpic,
    serviceEpics.servicesRequestEpic
);

const epicMiddlware = createEpicMiddleware();

const store = createStore(reducer, applyMiddleware(epicMiddlware));

epicMiddlware.run(epic);

export default store;
