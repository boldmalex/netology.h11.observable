import { ofType } from "redux-observable";
import { of, map, concatMap, catchError } from "rxjs";
import { ajax } from "rxjs/ajax";
import serviceActionTypes from "./service-action-types";
import servicesActions from "./service-actions";

const url = "http://localhost:7070/api/services";

const servicesRequestEpic = (action$) =>
  action$.pipe(
    ofType(serviceActionTypes.SERVICES_REQUEST),
    concatMap((o) =>
      ajax.getJSON(`${url}`).pipe(
        map((items) => servicesActions.servicesSuccess(items)),
        catchError((error) => of(servicesActions.servicesFailure(error)))
      )
    )
  );

const serviceRequestEpic = (action$) =>
  action$.pipe(
    ofType(serviceActionTypes.SERVICE_REQUEST),
    map(o => o.payload.id),
    concatMap((o) =>
      ajax.getJSON(`${url}/${o}`).pipe(
        map((item) => servicesActions.serviceSuccess(item)),
        catchError((error) => of(servicesActions.serviceFailure(error)))
      )
    )
  );

  const serviceEpics = {
    servicesRequestEpic,
    serviceRequestEpic
  }

export default serviceEpics;
