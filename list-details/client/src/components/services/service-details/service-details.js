import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import servicesActions from "../../../store/services/service-actions";
import { useParams } from "react-router-dom";
import ErrorMessage from "../../error-message/error-message";
import { SpinnerCircularSplit } from "spinners-react";

const ServiceDetails = () => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const store = useSelector((store) => store.serviceReducer);

  const loadData = () => {
    dispatch(servicesActions.serviceRequest(id));
  };

  useEffect(() => {
    loadData();
  }, []);

  if (store.isLoading) return <SpinnerCircularSplit/>;
  else if (store.error)
    return (
      <ErrorMessage
        text="Произошла ошибка!"
        btnText="Повторить запрос"
        btnHandler={loadData}
      />
    );
  else if (!store.item) return null;
  else
    return (
      <div>
        <div>
          <input type="text" readOnly={true} value={store.item.id}></input>
        </div>
        <div>
          <input type="text" readOnly={true} value={store.item.name}></input>
        </div>
        <div>
          <input type="text" readOnly={true} value={store.item.price}></input>
        </div>
        <div>
          <input type="text" readOnly={true} value={store.item.content}></input>
        </div>
      </div>
    );
};

export default ServiceDetails;
