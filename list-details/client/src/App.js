import react from "react";
import { Provider } from "react-redux";
import ServicesList from "./components/services/service-list/service-list";
import store from "./store/store";
import "./App.css";
import ServiceDetails from "./components/services/service-details/service-details";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";


function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes>
          <Route path="/" element={<ServicesList/>}/>
          <Route path="/:id/details" element={<ServiceDetails/>}/>
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
