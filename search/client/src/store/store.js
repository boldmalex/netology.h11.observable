import { applyMiddleware, createStore } from "redux";
import { combineEpics, createEpicMiddleware } from "redux-observable";
import searchReducer from "./search/search-reducer";
import searchEpics from "./search/search-epics";


const epic = combineEpics(
    searchEpics.searchChangeEpic,
    searchEpics.searchRequestEpic
);

const epicMiddlware = createEpicMiddleware();

const store = createStore(searchReducer, applyMiddleware(epicMiddlware));

epicMiddlware.run(epic);

export default store;