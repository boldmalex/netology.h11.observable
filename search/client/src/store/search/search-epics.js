import searchActions from "./search-actions";
import { ofType } from "redux-observable";
import {of, map, filter, debounceTime, switchMap, catchError } from "rxjs";
import { ajax } from "rxjs/ajax";
import searchActionTypes from "./search-action-types.js";

const url = 'http://localhost:7070/api/search';

const searchChangeEpic = (action$) =>
  action$.pipe(
    ofType(searchActionTypes.SEARCH_CHANGE),
    map((o) => o.payload.search.trim()),
    filter((o) => o !== ""),
    debounceTime(100),
    map((o) => searchActions.searchRequest(o))
  );



const searchRequestEpic = (action$) =>
  action$.pipe(
    ofType(searchActionTypes.SEARCH_REQUEST),
    map((o) => o.payload.search),
    map((o) => new URLSearchParams({ q: o })),
    switchMap((o) => ajax.getJSON(`${url}?${o}`).pipe(
      map((o) => searchActions.searchSuccess(o)),
      catchError(error => of(searchActions.searchFailure(error)))
    ))
  );


  const searchEpics = {
      searchChangeEpic,
      searchRequestEpic
  };

  export default searchEpics;
