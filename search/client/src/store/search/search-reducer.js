import searchActionTypes from "./search-action-types";

const initialState = {
  items: [],
  error: null,
  search: "",
  isLoading: false,
};

const searchReducer = (state = initialState, action) => {
  switch (action.type) {

    case searchActionTypes.SEARCH_CHANGE:
      const { search } = action.payload;
      return { ...state, search: search, items: search === "" ? [] : state.items };

    case searchActionTypes.SEARCH_SUCCESS:
      if (state.search !== "") {
        const { items } = action.payload;
        return { ...state, error: null, items: items, isLoading: false };
      } else 
        return { ...state, error: null, items: [], isLoading: false };

    case searchActionTypes.SEARCH_FAILURE:
      const { error } = action.payload;
      return { ...state, error: error, isLoading: false };

    case searchActionTypes.SEARCH_REQUEST:
      return { ...state, error: null, isLoading: true };

    default:
      return state;
  }
};

export default searchReducer;
