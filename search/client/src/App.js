import react from "react";
import { Provider } from "react-redux";
import SearchResult from "./components/search-result/search-result";
import Search from "./components/search/search";
import store from "./store/store";
import "./App.css";


function App() {
  return (
      <Provider store={store}>
        <Search/>
        <SearchResult/>
      </Provider>
  );
}

export default App;
