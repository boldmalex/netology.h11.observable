import react, { useState } from "react";
import { useDispatch } from "react-redux";
import searchActions from "../../store/search/search-actions";

const Search = () => {
  const dispatch = useDispatch();
  const [searchText, setSearchText] = useState("");

  const onSearchChange = ({ target }) => {
      setSearchText(target.value);
    dispatch(searchActions.searchChange(target.value));
  };

  return (
    <div>
      <input
        type="text"
        name="search"
        id="search"
        value={searchText}
        onChange={onSearchChange}
      ></input>
    </div>
  );
};

export default Search;
