import react from "react";
import { useSelector } from "react-redux";


const SearchResult = () => {
  const searchState = useSelector((store) => store);

  const getItems = () => {
    return searchState.items;
  };

  const showItem = (item) => {
    return <li key={item.id}>{item.name}</li>;
  };

  if (searchState.isLoading)
    return <div>Loading...</div>
  else if (searchState.items.length === 0 && searchState.search !== '')
    return <div>Nothing found</div>
  else if (searchState.items.length === 0)
    return <div>Type something to search</div>
  else if (searchState.items)
    return <ul className={searchState.error ? "error" : ""}>
              {getItems().map((item) => showItem(item))}
            </ul>;
  else 
    return null;
};

export default SearchResult;
